﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public GameObject currentCheckpoint;                        // Reference to the Checkpoint Game Object

    private GameObject _player;                                 // Reference to the Player Game Object

    private void Awake()
    {
        // Setting up the references
        _player = GameObject.FindGameObjectWithTag("Player");
    }

    public void RespawnPlayer()
    {
        // Spawn Player at the checkpoint's position
        _player.transform.position = currentCheckpoint.transform.position;

        // Turn on the user control script
        _player.GetComponent<Platformer2DUserControl>().enabled = true;

        // Enable player's colliders
        _player.GetComponent<BoxCollider2D>().enabled = true;
        _player.GetComponent<CircleCollider2D>().enabled = true;
    }
} 

