﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private LevelManager _levelManager;                     // Reference to the LevelManager script
    private Animator _animator;                             // Reference to the Animator component
    private bool _activated;                                // Whether or not the checkpoint is activated

    private void Awake()
    {
        // Setting up the references
        _levelManager = FindObjectOfType<LevelManager>();
        _animator = GetComponent<Animator>();
    }

    private void Start()
    {
        _activated = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // When the player collides with checkpoint and checkpoint wasn't activated already
        if(other.gameObject.CompareTag("Player") && !_activated)
        {
            // This checkpoint becomes new checkpoint to player's respawn
            _levelManager.currentCheckpoint = gameObject;

            // Set the flag to true so the player can't activate it more than one time
            _activated = true;

            // Play the checkpoint animation
            _animator.SetTrigger("Activated");
        }
    }
}
