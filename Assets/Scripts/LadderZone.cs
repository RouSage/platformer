﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderZone : MonoBehaviour
{
    private PlatformerCharacter2D _player;

    private void Start()
    {
        _player = FindObjectOfType<PlatformerCharacter2D>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name == "Player")
        {
            _player.onLadder = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.name == "Player")
        {
            _player.onLadder = false;
        }
    }
}
