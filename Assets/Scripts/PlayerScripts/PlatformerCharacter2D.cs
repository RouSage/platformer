using System;
using UnityEngine;

public class PlatformerCharacter2D : MonoBehaviour
{
    [HideInInspector] public bool onLadder;             // Whether of not the player is in the ladder zone
    public float climbSpeed;                            // Ladder climbing speed

    private float _climbVelocity;
    private float _gravityStore;                        // Store what gravity is on the player
    [SerializeField] private float maxSpeed = 10f;            // The fastest the player can travel in the x axis.
    [SerializeField] private float jumpForce = 15f;           // Amount of force added when the player jumps.
    [SerializeField] private bool hasAirControl = false;      // Whether or not the player can steer while jumping;
    [SerializeField] private LayerMask whatIsGround;          // A mask determining what is ground to the character

    private Transform _groundCheck;             // A position marking where to check if the player is grounded.
    const float _groundedRadius = .2f;          // Radius of the overlap circle to determine if grounded
    private bool _grounded;                     // Whether or not the player is grounded.
                                    
    private Animator _anim;                     // Reference to the player's animator component.
    private Rigidbody2D _rigidbody2D;           // Reference to the Rigidbody2D component
    private BoxCollider2D _boxCollider2D;       // Reference to the player's BoxCollider2D component
    private CircleCollider2D _circleCollider2D; // Reference to the player's CircleCollider2D component
    private bool _isFacingRight = true;         // For determining which way the player is currently facing.

    private void Awake()
    {
        // Setting up references.
        _groundCheck = transform.Find("GroundCheck");
        _anim = GetComponent<Animator>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _boxCollider2D = GetComponent<BoxCollider2D>();
        _circleCollider2D = GetComponent<CircleCollider2D>();
    }

    private void Start()
    {
        _gravityStore = _rigidbody2D.gravityScale;
    }

    private void FixedUpdate()
    {
        _grounded = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider2D[] colliders = Physics2D.OverlapCircleAll(_groundCheck.position, _groundedRadius, whatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
                _grounded = true;
        }
        _anim.SetBool("Ground", _grounded);

        // Set the vertical animation
        _anim.SetFloat("vSpeed", _rigidbody2D.velocity.y);
    }

    public void Move(float move, bool crouch, bool jump)
    {
        // If the player is crouching then he can't move
        // and his colliders' size and offsets is reducing so he's not floating above the ground
        if (crouch && _grounded)
        {
            move *= 0;
            transform.position = new Vector3(transform.position.x, transform.position.y - 0.07f, transform.position.z);
            _circleCollider2D.offset = new Vector2(-0.04f, -0.4f);
            _boxCollider2D.offset = new Vector2(0f, 0f);
            _boxCollider2D.size = new Vector2(1.3f, 0.9f);
        }
        else
        {
            _circleCollider2D.offset = new Vector2(-0.04f, -0.62f);
            _boxCollider2D.offset = new Vector2(0f, 0.12f);
            _boxCollider2D.size = new Vector2(0.86f, 1.26f);
        }

        // Set whether or not the character is crouching in the animator
        _anim.SetBool("Crouch", crouch);

        // Only control the player if he's grounded or airControl is turned on and the player
        if (_grounded || hasAirControl)
        {
            // The Speed animator parameter is set to the absolute value of the horizontal input.
            _anim.SetFloat("Speed", Mathf.Abs(move));

            // Move the character
            _rigidbody2D.velocity = new Vector2(move * maxSpeed, _rigidbody2D.velocity.y);

            // If the input is moving the player right and the player is facing left...
            if (move > 0 && !_isFacingRight)
            {
                // ... flip the player.
                Flip();
            }
            // Otherwise if the input is moving the player left and the player is facing right...
            else if (move < 0 && _isFacingRight)
            {
                // ... flip the player.
                Flip();
            }
        }

        // If the player should jump...
        if (_grounded && jump && !crouch && _anim.GetBool("Ground"))
        {
            Jump(jumpForce);
        }

        // If the player is on ladder
        if(onLadder)
        {
            // Set his gravity to 0 so he won't fall down
            _rigidbody2D.gravityScale = 0f;

            _climbVelocity = climbSpeed * Input.GetAxisRaw("Vertical");

            _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, _climbVelocity);
        }

        if(!onLadder)
        {
            _rigidbody2D.gravityScale = _gravityStore;
        }
    }

    private void Jump(float jumpForce)
    {
        _grounded = false;
        // The Ground animator parameter is set to false, so the player isn't grounded
        _anim.SetBool("Ground", false);
        // Add a vertical force to the player's velocity.
        _rigidbody2D.velocity = jumpForce * Vector2.up;
    }

    // Flipping the player
    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        _isFacingRight = !_isFacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    // Picking up objects
    private void OnTriggerEnter2D(Collider2D other)
    {
        // If some game object has a Tag 'Collectable'
        if (other.gameObject.CompareTag("Collectable"))
        {
            // Player will collect it and object is destroyed
            Destroy(other.gameObject);
        }
    }
}
