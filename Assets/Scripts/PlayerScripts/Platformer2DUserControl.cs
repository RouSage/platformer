using System;
using UnityEngine;

[RequireComponent(typeof(PlatformerCharacter2D))]
public class Platformer2DUserControl : MonoBehaviour
{
    private PlatformerCharacter2D _character;
    private bool _isJump;

    private void Awake()
    {
        _character = GetComponent<PlatformerCharacter2D>();
    }

    private void Update()
    {
        if (!_isJump)
        {
            // Read the jump input in Update so button presses aren't missed.
            _isJump = Input.GetButtonDown("Jump");
        }
    }

    private void FixedUpdate()
    {
        // Read the inputs.
        bool crouch = Input.GetKey(KeyCode.LeftControl);
        float h = Input.GetAxis("Horizontal");
        // Pass all parameters to the character control script.
        _character.Move(h, crouch, _isJump);
        _isJump = false;
    }
}
