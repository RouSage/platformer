﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public const int maxHealth = 3;                         // The maximum amount of the player's health
    public int currentHealth;                               // Current player's health

    private LevelManager _levelManager;                         // Reference to the LevelManager
    private Animator _anim;                                     // Reference to the Animator component
    private Platformer2DUserControl _platformer2DUserControl;   // Reference to the Platformer2DUserControl script
    private BoxCollider2D _boxCollider2D;                       // Reference to the BoxCollider2D component
    private CircleCollider2D _circleCollider2D;                 // Reference to the CircleCollider2D component
    private bool _isDead;                                       // Is the player dead or not?
    private bool _isDamaged;                                    // True when the player gets damaged

    private void Awake()
    {
        // Setting up the references
        _anim = GetComponent<Animator>();
        _platformer2DUserControl = GetComponent<Platformer2DUserControl>();
        _levelManager = FindObjectOfType<LevelManager>();
        _boxCollider2D = GetComponent<BoxCollider2D>();
        _circleCollider2D = GetComponent<CircleCollider2D>();
    }

    private void Start()
    {
        // Set the initial health of the player
        currentHealth = maxHealth;
    }

    private void Update()
    {
        // If the player has just been damaged...
        if (_isDamaged)
        {
            // ...play the hurt animation
            _anim.SetBool("Damaged", _isDamaged);
        }
        else
        {
            _anim.SetBool("Damaged", false);
        }

        _isDamaged = false;
    }

    public void ReceiveDamage(int amount)
    {
        // Set the isDamaged flag to true so the animation will be played
        _isDamaged = true;

        // Reduce the current health by the damage amount
        currentHealth -= amount;

        // If the player has lost all its health and the death flag hasn't been set yet...
        if(currentHealth <= 0 && !_isDead)
        {
            //... it should die
            Death();
        }
    }

    private void Death()
    {
        // Set the death flag so this function won't be called again
        _isDead = true;

        // Turn off the user control script
        _platformer2DUserControl.enabled = false;

        // Decrease player's velocity so he will spawn correctly
        gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;

        // Play the idle animation
        _anim.Play("PlayerIdle");

        // Disable player's colliders
        _boxCollider2D.enabled = false;
        _circleCollider2D.enabled = false;

        // Player spawns again
        _levelManager.RespawnPlayer();
    }
}
