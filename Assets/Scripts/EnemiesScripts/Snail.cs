﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snail : MonoBehaviour
{
    private Animator _anim;                     // Reference to the Animator component
    private Rigidbody2D _rigidbody2D;           // Reference to the Rigidbody2D component
    private EnemyMovement _enemyMovement;       // Reference to the EnemyMovement script
    private HitPlayer _hitPlayer;               // Reference to the HitPlayer script
    private bool _isDamaged;                    // Check if the snail was damaged

    private void Awake()
    {
        // Setting up the references
        _anim = GetComponent<Animator>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _enemyMovement = GetComponent<EnemyMovement>();
        _hitPlayer = GetComponent<HitPlayer>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // When the player is above the snail and falling down on him
        if (other.GetComponent<Rigidbody2D>() != null && other.GetComponent<Rigidbody2D>().velocity.y < 0)
        {
            // Snail hides in its shell
            Hide();
        }
    }

    private void Hide()
    {
        // Set the isDamaged flag to true
        _isDamaged = true;

        // Play the hide-in-shell animation
        _anim.SetBool("Damaged", _isDamaged);

        // Stop the snail
        _rigidbody2D.velocity = Vector2.zero;

        // Turn off the movement script
        _enemyMovement.enabled = false;

        // Turn off the HitPlayer script
        _hitPlayer.enabled = false;

        StartCoroutine("WaitForReturn");
    }

    private IEnumerator WaitForReturn()
    {
        yield return new WaitForSeconds(5f);

        // Set the isDamaged flag to false
        _isDamaged = false;

        // Return to walk
        _anim.SetBool("Damaged", _isDamaged);

        // Turn on the movement script
        _enemyMovement.enabled = true;

        // Turn on the HitPlayer script
        _hitPlayer.enabled = true;
    }
}
