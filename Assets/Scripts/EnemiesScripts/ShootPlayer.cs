﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootPlayer : MonoBehaviour
{
    public float fireRate = 1.5f;
    public int damage = 1;
    public Transform firePoint;
    public GameObject fireBall;

    private float _timer;

    private void Update()
    {
        // Add the time since Update was last called to the timer
        _timer += Time.deltaTime;

        if(_timer >= fireRate)
        {
            _timer = 0f;
            Instantiate(fireBall, firePoint.position, firePoint.rotation);
        }
    }
}
