﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float moveSpeed;                 // The speed of the enemy
    public LayerMask whatToCheck;           // Where the enemy can move

    private Rigidbody2D _rigidbody2D;       // Reference to the Rigidbody2D component
    private float _objectWidth;             // Width of the enemy sprite (on the x-axis)
    private float _objectHeight;            // Height of the enemy sprite (the enemy won't lag at the initial position)

    private void Awake()
    {
        // Setting up the references and the enemy's width and height
        _rigidbody2D = GetComponent<Rigidbody2D>();

        _objectWidth = GetComponent<SpriteRenderer>().bounds.extents.x;
        _objectHeight = GetComponent<SpriteRenderer>().bounds.extents.y;
    }

    private void FixedUpdate()
    {
        // Get the LineCast position to check if there's ground in front of the enemy before moving forward (it's staight before the enemy)
        Vector2 lineCastPosition = transform.position.toVector2() - transform.right.toVector2() * _objectWidth + Vector2.up * _objectHeight;
        Debug.DrawLine(lineCastPosition, lineCastPosition + Vector2.down);
        Debug.DrawLine(lineCastPosition, lineCastPosition - transform.right.toVector2() * 0.05f);

        // Check if the enemy is on the ground 
        bool isGrounded = Physics2D.Linecast(lineCastPosition, lineCastPosition + Vector2.down, whatToCheck);

        // Check if there's is something in front of the enemy that hinders its moving
        bool isBlocked = Physics2D.Linecast(lineCastPosition, lineCastPosition - transform.right.toVector2() * 0.05f, whatToCheck);

        // If there's no ground or the enemy is blocked...
        if (!isGrounded || isBlocked)
        {
            //... turn around
            Vector3 currentRotation = transform.eulerAngles;
            currentRotation.y += 180;
            transform.eulerAngles = currentRotation;
        }

        // Always move forward
        Vector2 newVelocity = _rigidbody2D.velocity;
        newVelocity.x = -transform.right.x * moveSpeed;
        _rigidbody2D.velocity = newVelocity;
    }
}
