﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPlayer : MonoBehaviour
{
    public const float timeBetweenHits = 1.5f;      // The time in seconds between each hit
    public int hitDamage = 1;                       // The amount of damage the player will receive

    private GameObject _player;                     // Reference to the player GameObject
    private PlayerHealth _playerHealth;             // Reference to the player's health (script)
    private bool _playerInRange;                    // Whether player is within the trigger collider and can be damaged.
    private float _timer;                           // Timer for counting up to the next hit

    private void Awake()
    {
        // Setting up the references
        _player = GameObject.FindGameObjectWithTag("Player");
        _playerHealth = _player.GetComponent<PlayerHealth>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // If the entering collider is the player...
        if(other.gameObject == _player)
        {
            // ... the player is in range
            _playerInRange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        // If the exiting collider is the player...
        if(other.gameObject == _player)
        {
            // ... the player is no longer in range
            _playerInRange = false;
        }
    }

    private void Update()
    {
        // Add the time since Update was last called to the timer
        _timer += Time.deltaTime;

        // If the timer exceeds the time between hits and the player is in range...
        if(_timer >= timeBetweenHits && _playerInRange)
        {
            //... hit
            Hit();
        }
    }

    private void Hit()
    {
        // Reset the timer
        _timer = 0f;

        // If the player has health to lose...
        if(_playerHealth.currentHealth > 0)
        {
            // ... hit the player
            _playerHealth.ReceiveDamage(hitDamage);
        }
    }
}
