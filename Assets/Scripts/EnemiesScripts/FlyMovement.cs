﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyMovement : MonoBehaviour
{
    public float moveSpeed;                                 // The speed of the fly
    public float maxDistance;                               // Max distance to move

    private bool dirRight = true;                           // Is fly moving right direction?
    private SpriteRenderer _spriteRenderer;                 // Reference to the SpriteRenderer component
    private Animator _anim;                                 // Reference to the Animator component
    private HitPlayer _hitPlayer;                           // Reference to the HitPlayer script

    private void Awake()
    {
        // Setting up the references
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _anim = GetComponent<Animator>();
        _hitPlayer = GetComponent<HitPlayer>();
    }

    private void Start()
    {
        // Max distance is equal to origin fly position plus some value
        maxDistance += transform.position.x;
        // Flip sprite to the right direction
        _spriteRenderer.flipX = true;
    }

    void Update()
    {
        // If fly is watching to the right
        if(dirRight)
            // Fly moves to the right
            transform.Translate(Vector2.right * moveSpeed * Time.deltaTime);
        else
            // Else moves to the left
            transform.Translate(-Vector2.right * moveSpeed * Time.deltaTime);

        if(transform.position.x >= maxDistance)
        {
            dirRight = false;
            _spriteRenderer.flipX = false;
        }

        if(transform.position.x <= -maxDistance)
        {
            dirRight = true;
            _spriteRenderer.flipX = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // When the player is above the enemy and falling down on him
        if(other.GetComponent<Rigidbody2D>() != null && other.GetComponent<Rigidbody2D>().velocity.y < 0)
        {
            // The enemy is dead
            Death();
        }
    }

    private void Death()
    {
        // Play the enemy's death animation 
        _anim.SetTrigger("Dead");

        // Turn off the movement script
        this.enabled = false;

        //Turn off the HitPlayer script
        _hitPlayer.enabled = false;

        Destroy(gameObject, 0.2f);
    }
}

