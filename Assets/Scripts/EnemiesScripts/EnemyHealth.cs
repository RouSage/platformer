﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    private Animator _anim;                                 // Reference to the Animator component
    private Rigidbody2D _rigidbody2D;                       // Reference to the Rigidbody2D component
    private EnemyMovement _enemyMovement;                   // Reference to the enemy's movement script
    private HitPlayer _hitPlayer;                           // Reference to the HitPlayer script
    private float _waitBeforeDestroy = 2f;                  // Time to wait before destroying the object (enemy)

    private void Awake()
    {
        // Setting up the references
        _anim = GetComponent<Animator>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _enemyMovement = GetComponent<EnemyMovement>();
        _hitPlayer = GetComponent<HitPlayer>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // When the player is above the enemy and falling down on him
        if(other.GetComponent<Rigidbody2D>() != null && other.GetComponent<Rigidbody2D>().velocity.y < 0)
        {
            // The enemy is dead
            Death();
        }
    }

    private void Death()
    {
        // Play the enemy's death animation 
        _anim.SetTrigger("Dead");

        // Stop the enemy from moving
        _rigidbody2D.velocity = Vector2.zero;

        // Turn off the movement script
        _enemyMovement.enabled = false;

        //Turn off the HitPlayer script
        _hitPlayer.enabled = false;

        Destroy(gameObject, _waitBeforeDestroy);
    }
}