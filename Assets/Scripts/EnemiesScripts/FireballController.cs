﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballController : MonoBehaviour
{
    public float speed;                       // Fireball speed
    public int damage = 1;                    // Amount of damage that fireball does

    private GameObject _player;                 // Reference to the player's GameObject
    private PlayerHealth _playerHealth;         // Reference to the PlayerHealth script

    private void Awake()
    {
        // Setting up the references
        _player = GameObject.FindGameObjectWithTag("Player");
        _playerHealth = _player.GetComponent<PlayerHealth>();
    }

    private void Update()
    {
        // The fireball flies to the left
        transform.Translate(-Vector2.right * speed * Time.deltaTime);

        // Destroy fireball after 5 seconds (if it hasn't hit anything)
        Destroy(gameObject, 5f);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // If fireball hits player
        if(other.gameObject == _player)
        {
            // And the player's health isn't equal to 0
            if(_playerHealth.currentHealth > 0)
            {
                // Player takes damage
                _playerHealth.ReceiveDamage(damage);
            }
        }

        // Firebal gets destroyed
        Destroy(gameObject);
    }
}
