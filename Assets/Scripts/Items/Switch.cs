﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{
    // Sprites for every state of the switch
    public Sprite switchMid;
    public Sprite switchLeft;
    public Sprite switchRight;
    // Object which depends from this switch
    public GameObject dependentObject;
    // Player's position to detect whether it's near the switch
    public Transform playerPosition;

    private SwitchStates _currentState;                         // Variable for every state of the switch
    private SpriteRenderer _spriteRenderer;                     // Reference to the SpriteRenderer component

    private void Awake()
    {
        // Setting up the references
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        // Initialization
        _spriteRenderer.sprite = switchMid;
        _currentState = SwitchStates.Mid;
    }

    private void Update()
    {
        // If the player is near the switch
        if(Mathf.Abs(transform.position.x - playerPosition.position.x) <= 1)
        {
            // Get the current switch state
            GetInput();
            // And move dependentObject to a certain direction
            MoveObject();
        }
    }

    /// <summary>
    /// Moves dependentObject to a certain direction (left, right)
    /// </summary>
    private void MoveObject()
    {
        // Find out in what state the switch is
        switch(_currentState)
        {
            // TODO: Move dependentObject somehow
            case SwitchStates.Mid:
                {
                    break;
                }
            case SwitchStates.Left:
                {
                    break;
                }
            case SwitchStates.Right:
                {
                    break;
                }
        }
    }

    /// <summary>
    /// Changes the current switch state when arrow buttons was pressed
    /// </summary>
    private void GetInput()
    {
        // If left arrow was pressed and the state is Mid
        if(Input.GetKeyDown(KeyCode.LeftArrow) && _currentState == SwitchStates.Mid)
        {
            // Change the current sprite to the left state
            _spriteRenderer.sprite = switchLeft;

            // Change the current state to the left
            _currentState = SwitchStates.Left;
        }
        // If left arrow was pressed and the state is Right
        if(Input.GetKeyDown(KeyCode.LeftArrow) && _currentState == SwitchStates.Right)
        {
            // Change sprite to the mid state
            _spriteRenderer.sprite = switchMid;

            // Change the current state to Mid
            _currentState = SwitchStates.Mid;
        }
        // If right arrow was pressed and the state is Mid
        if(Input.GetKeyDown(KeyCode.RightArrow) && _currentState == SwitchStates.Mid)
        {
            // Change the current sprite to the right state
            _spriteRenderer.sprite = switchRight;

            // Change the current state to the right
            _currentState = SwitchStates.Right;
        }
        // If right arrow was presse and the state is Left
        if(Input.GetKeyDown(KeyCode.RightArrow) && _currentState == SwitchStates.Left)
        {
            // Change sprite to the mid state
            _spriteRenderer.sprite = switchMid;

            // Change the current state to Mid
            _currentState = SwitchStates.Mid;
        }
    }
}

enum SwitchStates
{
    Mid,
    Left,
    Right
}
