﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Springboard : MonoBehaviour
{
    public GameObject springboardUp;        // Reference to the SpringboardUp game object
    public GameObject springboardDown;      // Reference to the SpringboardDown game object
    public float bounceFactor = 20f;        // The force that will be applied to the player

    private BoxCollider2D _boxCollider2D;   // Reference to the BoxCollider2D component

    private void Awake()
    {
        // Setting up the references
        _boxCollider2D = GetComponent<BoxCollider2D>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // When player is falling down on the springboard...
        if (other.GetComponent<Rigidbody2D>() != null && other.GetComponent<Rigidbody2D>().velocity.y < 0)
        {
            // ... change the sprites (game objects) and disable boxCollider...
            StartCoroutine("ChangeSpringboardState");

            // ... and then player will jump
            other.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            other.GetComponent<Rigidbody2D>().AddForce(Vector2.up * bounceFactor, ForceMode2D.Impulse);
        }
    }

    private IEnumerator ChangeSpringboardState()
    {
        // The springboard's state is unactive
        springboardUp.SetActive(false);
        springboardDown.SetActive(true);

        // Disable the boxCollider component so the player can't jump while the springboard is uncatvie
        _boxCollider2D.enabled = false;

        // The springboard is unactive for 1 second
        yield return new WaitForSeconds(1f);

        // The springboard's state is active
        springboardUp.SetActive(true);
        springboardDown.SetActive(false);

        // The player can jump again
        _boxCollider2D.enabled = true;
    }
}
