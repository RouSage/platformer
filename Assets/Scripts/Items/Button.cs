﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    public List<GameObject> whatToActivate;         // What objects will be activated when the button is pressed
    public Sprite notPressed;                       // Reference to the initial sprite of the button
    public Sprite pressed;                          // Reference to the sprite of the pressed button

    private SpriteRenderer _spriteRenderer;         // Reference to the SpriteRenderer component
    private CapsuleCollider2D _capsuleCollider2d;   // Reference to the CapsuleCollider2D component

    private void Awake()
    {
        // Setting up the references
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _capsuleCollider2d = GetComponent<CapsuleCollider2D>();
    }

    private void Start()
    {
        // The initial state of the button when it's not pressed
        _spriteRenderer.sprite = notPressed;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // If the player interacts with the button (or smth else too maybe?)
        if(other.gameObject.tag == "Player")
        {
            // Change sprite to a pressed button
            _spriteRenderer.sprite = pressed;

            foreach (var item in whatToActivate)
            {
                if(item.gameObject.GetComponent<Lock>())
                {
                    item.gameObject.GetComponent<Lock>().Unlock();
                }
            }

            // Player can't interact with the button anymore
            _capsuleCollider2d.enabled = false;
        }
    }
}
