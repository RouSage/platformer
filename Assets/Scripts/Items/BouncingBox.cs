﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncingBox : MonoBehaviour
{
    public List<GameObject> containedItems;                 // List of the items the box contains
    public Sprite enabledBox;                               // Sprite for the enabled box state
    public Sprite disabledBox;                              // Sprite for the disabled box state
    public bool _isInteractable;                            // True if this box can drop something

    private SpriteRenderer _spriteRenderer;                 // Reference to the SpriteRenderer component

    private void Awake()
    {
        // Setting up the references
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _spriteRenderer.sprite = null;
    }

    private void Start()
    {
        // Initial state of any box is enabled
        _spriteRenderer.sprite = enabledBox;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // If player hits box with his head...
        if(other.GetComponent<Rigidbody2D>().velocity.y > 0 && other.gameObject.transform.position.y < transform.position.y && other.gameObject.tag == "Player")
        {
            // ... player stops in the air and falls down
            other.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

            // Box makes a little bounce cause of the player's hit
            StartCoroutine(Bounce(transform.position));

            // If the box is interactable...
            if (_isInteractable)
            {
                // ... set the flag to false so the player can't interact with the box anymore...
                _isInteractable = false;
              
                // ... and drop a random item
                int itemNumber = ChooseItem(containedItems);
                Instantiate(
                    containedItems[itemNumber],
                    new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z),
                    Quaternion.identity);

                // Change the sprite from enabledBox to disabledBox
                _spriteRenderer.sprite = disabledBox;
            }
        }
    }

    private IEnumerator Bounce(Vector3 initialPosition)
    {
        // Disable the isTrigger property for a while so the box won't jump often
        gameObject.GetComponent<BoxCollider2D>().isTrigger = false;

        // The box moves up a little and stays there for 0.08 seconds
        transform.position = new Vector3(initialPosition.x, initialPosition.y + 0.5f, initialPosition.z);
        yield return new WaitForSeconds(0.08f);

        // The box returns to its initial position
        transform.position = new Vector3(initialPosition.x, initialPosition.y, initialPosition.z);
        yield return new WaitForSeconds(0.25f);

        // Player can make the box bounce again after 0.25 seconds
        gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
    }

    /// <summary>
    /// Return the random number in some range
    /// </summary>
    /// <param name="items"></param>
    /// <returns></returns>
    private int ChooseItem(List<GameObject> items)
    {
        return Random.Range(0, items.Count);
    }
}
